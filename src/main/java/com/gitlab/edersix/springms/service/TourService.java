package com.gitlab.edersix.springms.service;

import com.gitlab.edersix.springms.domain.Difficulty;
import com.gitlab.edersix.springms.domain.Region;
import com.gitlab.edersix.springms.domain.Tour;
import com.gitlab.edersix.springms.domain.TourPackage;
import com.gitlab.edersix.springms.repository.TourPackageRepository;
import com.gitlab.edersix.springms.repository.TourRepository;
import org.springframework.stereotype.Service;

@Service
public class TourService {
    private TourRepository tourRepository;
    private TourPackageRepository tourPackageRepository;

    public TourService(TourRepository tourRepository, TourPackageRepository tourPackageRepository) {
        this.tourRepository = tourRepository;
        this.tourPackageRepository = tourPackageRepository;
    }

    /**
     *
     * @param title
     * @param description
     * @param blurb
     * @param price
     * @param duration
     * @param bullets
     * @param keywords
     * @param tourPackageName
     * @param difficulty
     * @param region
     * @return
     */
    public Tour createTour(String title, String description, String blurb,
                           Integer price, String duration, String bullets,
                           String keywords, String tourPackageName,
                           Difficulty difficulty, Region region)
    {
        TourPackage tourPackage=this.tourPackageRepository.findByName(tourPackageName)
                .orElseThrow(() -> new RuntimeException("Tour package does not exist"
                +tourPackageName));

        return  this.tourRepository.save(
                new Tour(title,description,blurb,price,duration,bullets,
                        keywords,tourPackage,difficulty,region));
    }

    /**
     *
     * @return
     */
    public long total(){
        return this.tourRepository.count();
    }

}
