package com.gitlab.edersix.springms.service;

import com.gitlab.edersix.springms.domain.TourPackage;
import com.gitlab.edersix.springms.repository.TourPackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TourPackageService {
    private TourPackageRepository tourPackageRepository;

    @Autowired
    public TourPackageService(TourPackageRepository tourPackageRepository){
        this.tourPackageRepository=tourPackageRepository;
    }

    /**
     *
     * @param code
     * @param name
     * @return
     */
    public TourPackage createTourPackage(String code, String name){
        return  this.tourPackageRepository.findById(code)
                .orElse(this.tourPackageRepository.
                        save(new TourPackage(code,name)));
    }

    /**
     *
     * @return
     */
    public Iterable<TourPackage> lookup(){
        return this.tourPackageRepository.findAll();
    }

    /**
     *
     * @return
     */
    public Long total(){
        return this.tourPackageRepository.count();
    }
}
