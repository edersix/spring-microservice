package com.gitlab.edersix.springms.repository;

import com.gitlab.edersix.springms.domain.Tour;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(collectionResourceRel = "packages",path = "packages")//renaming the end point tours->packages
public interface TourRepository extends PagingAndSortingRepository<Tour, Integer> {
//    List<Tour> findByTourPackageCode(@Param("code") String code);
    Page<Tour> findByTourPackageCode(@Param("code") String code, Pageable pageable);
    //?size=3
//    page=1
//    sort=title,asc


    @Override
    @RestResource(exported = false)//control access at method level
    <S extends Tour> S save(S entity);

    @Override
    @RestResource(exported = false)//control access at method level
    <S extends Tour> Iterable<S> saveAll(Iterable<S> entities);

    @Override
    @RestResource(exported = false)//control access at method level
    void deleteById(Integer integer);

    @Override
    @RestResource(exported = false)//control access at method level
    void delete(Tour entity);

    @Override
    @RestResource(exported = false)//control access at method level
    void deleteAllById(Iterable<? extends Integer> integers);

    @Override
    @RestResource(exported = false)//control access at method level
    void deleteAll(Iterable<? extends Tour> entities);

    @Override
    @RestResource(exported = false)//control access at method level
    void deleteAll();
}
