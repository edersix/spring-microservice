package com.gitlab.edersix.springms.repository;

import com.gitlab.edersix.springms.domain.TourPackage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;
//@RepositoryRestResource(exported = false)//control access at class level
public interface TourPackageRepository extends CrudRepository<TourPackage, String> {
    Optional<TourPackage> findByName(@Param("name") String name);

    @Override
    @RestResource(exported = false)//control access at method level
    <S extends TourPackage> S save(S entity);

    @Override
    @RestResource(exported = false)//control access at method level
    void delete(TourPackage entity);

    @Override
    @RestResource(exported = false)//control access at method level
    void deleteAllById(Iterable<? extends String> strings);

    @Override
    @RestResource(exported = false)//control access at method level
    void deleteAll(Iterable<? extends TourPackage> entities);

    @Override
    @RestResource(exported = false)//control access at method level
    void deleteAll();
}

