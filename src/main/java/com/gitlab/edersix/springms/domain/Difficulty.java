package com.gitlab.edersix.springms.domain;

public enum Difficulty {
    Easy, Medium, Difficult, Varies;
}
